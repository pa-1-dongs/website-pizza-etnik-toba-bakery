<?php include_once('koneksi/koneksi.php');?>
<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Pizza Etnik Toba Bakery</title>
	<link rel="icon" href="../Pict/PET.png" type="image/jpg">

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/responsive.css">
	<link rel="stylesheet" href="../css/navbar.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css"
		integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA=="
		crossorigin="anonymous" referrerpolicy="no-referrer" />
		<link href="https://fonts.googleapis.com/css2?family=Magra&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Delius+Unicase&display=swap" rel="stylesheet">

</head>

<!-- header -->
<?php include 'koneksi/header.php' ?>

<div id="pesansekarang">
<!-- Start Menu -->
<form action="" method="post">
<div class="menu-box">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="heading-title text-center">
					<h2>MENU</h2>
				</div>
			</div>
		</div>

		<div class="row inner-menu-box">
			<div class="col-3">
				<div class="container-input">
					<input type="text" placeholder="Search" name="search" class="input">
					<svg fill="#000000" width="20px" height="20px" viewBox="0 0 1920 1920"
						xmlns="http://www.w3.org/2000/svg" name ="cari">
						<path
							d="M790.588 1468.235c-373.722 0-677.647-303.924-677.647-677.647 0-373.722 303.925-677.647 677.647-677.647 373.723 0 677.647 303.925 677.647 677.647 0 373.723-303.924 677.647-677.647 677.647Zm596.781-160.715c120.396-138.692 193.807-319.285 193.807-516.932C1581.176 354.748 1226.428 0 790.588 0S0 354.748 0 790.588s354.748 790.588 790.588 790.588c197.647 0 378.24-73.411 516.932-193.807l516.028 516.142 79.963-79.963-516.142-516.028Z"
							fill-rule="evenodd"></path>
					</svg>
					<!-- <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true" name ="cari"></i></button> -->
				</div>
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-pills-semua-tab" data-toggle="pill" href="#v-pills-semua" role="tab" aria-controls="v-pills-semua" aria-selected="true">All</a>
						<a class="nav-link" id="v-pills-makanan-tab" data-toggle="pill" href="#v-pills-makanan" role="tab" aria-controls="v-pills-makanan" aria-selected="false">Makanan</a>
						<a class="nav-link" id="v-pills-minuman-tab" data-toggle="pill" href="#v-pills-minuman" role="tab" aria-controls="v-pills-minuman" aria-selected="false">Minuman</a>
						<a class="nav-link" id="v-pills-roti-tab" data-toggle="pill" href="#v-pills-roti" role="tab" aria-controls="v-pills-roti" aria-selected="false">Roti dan Kue</a>
				</div>
			</div>

			<div class="col-9">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-semua" role="tabpanel" aria-labelledby="v-pills-semua-tab">
							<div class="row">
					
			                  <?php 
							  if(isset($_POST['cari'])){
								$cari = $_POST['search'];
								$query = "SELECT * FROM produk WHERE status_produk='aktif' AND nama_produk  LIKE '$cari%'";
							  }else{
								$query = "SELECT * FROM produk WHERE status_produk='aktif'";
							  }
			                  $result_set = $koneksi->query($query);
			                  while ($row = $result_set->fetch_assoc()){
			                  ?>
			                  <div class="col-lg-4 col-md-6 special-grid drinks">
			                    <div class="gallery-single fix">
			                      <img src="../pictures/<?php echo $row['gambar_produk'];?>" class="img-fluid" alt="Image" />
			                      <div class="why-text">
			                        <h4><?php echo  $row['nama_produk'];?></h4>
			                        <p><?php echo  $row['deskripsi_produk'];?></p>
			                        <h5>Rp <?php echo number_format($row['harga_produk'],0,',','.');?></h5>
			                      </div>
			                    </div>
			                  </div>
			                  <?php } ?>
                
							</div>
							
						</div>
						<div class="tab-pane fade" id="v-pills-makanan" role="tabpanel" aria-labelledby="v-pills-makanan-tab">
							<div class="row">
							  <?php 
			                  $query = 'SELECT * FROM produk WHERE status_produk="aktif" AND id_kategori=1';
			                  $result_set = $koneksi->query($query);
			                  while ($row = $result_set->fetch_assoc()){
			                  ?>
			                  <div class="col-lg-4 col-md-6 special-grid drinks">
			                    <div class="gallery-single fix">
			                      <img src="../pictures/<?php echo $row['gambar_produk'];?>" class="img-fluid" alt="Image" />
			                      <div class="why-text">
			                        <h4><?php echo  $row['nama_produk'];?></h4>
			                        <p><?php echo  $row['deskripsi_produk'];?></p>
			                        <h5>Rp <?php echo number_format($row['harga_produk'],0,',','.');?></h5>
			                      </div>
			                    </div>
			                  </div>
			                  <?php } ?>
							</div>
							
						</div>
						<div class="tab-pane fade" id="v-pills-minuman" role="tabpanel" aria-labelledby="v-pills-minuman-tab">
							<div class="row">
								  <?php 
			                  $query = 'SELECT * FROM produk WHERE status_produk="aktif" AND id_kategori=2';
			                  $result_set = $koneksi->query($query);
			                  while ($row = $result_set->fetch_assoc()){
			                  ?>
			                  <div class="col-lg-4 col-md-6 special-grid drinks">
			                    <div class="gallery-single fix">
			                      <img src="../pictures/<?php echo $row['gambar_produk'];?>" class="img-fluid" alt="Image" />
			                      <div class="why-text">
			                        <h4><?php echo  $row['nama_produk'];?></h4>
			                        <p><?php echo  $row['deskripsi_produk'];?></p>
			                        <h5>Rp <?php echo number_format($row['harga_produk'],0,',','.');?></h5>
			                      </div>
			                    </div>
			                  </div>
			                  <?php } ?>
							</div>
							</div>
							<div class="tab-pane fade" id="v-pills-roti" role="tabpanel" aria-labelledby="v-pills-roti-tab">
							<div class="row">
							  <?php 
			                  $query = 'SELECT * FROM produk WHERE status_produk="aktif" AND id_kategori=3';
			                  $result_set = $koneksi->query($query);
			                  while ($row = $result_set->fetch_assoc()){
			                  ?>
			                  <div class="col-lg-4 col-md-6 special-grid drinks">
			                    <div class="gallery-single fix">
			                      <img src="../pictures/<?php echo $row['gambar_produk'];?>" class="img-fluid" alt="Image" />
			                      <div class="why-text">
			                        <h4><?php echo  $row['nama_produk'];?></h4>
			                        <p><?php echo  $row['deskripsi_produk'];?></p>
			                        <h5>Rp <?php echo number_format($row['harga_produk'],0,',','.');?></h5>
			                      </div>
			                    </div>
			                  </div>
			                  <?php } ?>
							</div>
						</div>
							<div class="tab-pane fade" id="v-pills-pesan" role="tabpanel" aria-labelledby="v-pills-pesan-tab">
							<div class="row">
							  <?php 
			                  $query = 'SELECT * FROM produk WHERE status_produk="aktif"';
			                  $result_set = $koneksi->query($query);
			                  while ($row = $result_set->fetch_assoc()){
			                  ?>
			                  <div class="col-lg-4 col-md-6 special-grid drinks">
			                    <div class="gallery-single fix">
			                      <img src="../pictures/<?php echo $row['gambar_produk'];?>" class="img-fluid" alt="Image" />
			                      <div class="why-text">
			                        <h4><?php echo  $row['nama_produk'];?></h4>
			                        <p><?php echo  $row['deskripsi_produk'];?></p>
			                        <h5>Rp <?php echo number_format($row['harga_produk'],0,',','.');?></h5>
			                      </div>
			                    </div>
			                      <input type="checkbox" id="pizza-natinuknuk" name="pesanan[]" value="Pizza Natinuknuk">pesan
			                  <input type="number" id="jumlah-pizza-natinuknuk" name="jumlah_pizza_natinuknuk" value="0" min="0" max="500" style="width: 40px;">
			                  </div>
			                  <?php } ?>
							</div>
							
						</div>
						</div>
						</div>
					</div>
				</div>
			</div>
<!-- End Menu -->
</form>

<center><button type="button" class="btn btn-warning" id="pesan" style="width:150px;font-family:magra;	">Pesan Sekarang</button></center>
							  </div>
<script>
var btnPesan = document.getElementById("pesan");
var pesansekarang = document.getElementById("pesansekarang");
btnPesan.addEventListener("click", function () {
  // alert('Anda sedang memesan sekarang!');
  var xhr = new XMLHttpRequest();

  // Cek kesiapan ajax
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4 && xhr.status == 200) {
      pesansekarang.innerHTML = xhr.responseText;
    }
  };

  // eksekusi ajax
  xhr.open("GET", "ajax/pesan_sekarang.php", true);
  xhr.send();
});
</script>

<!-- footer -->
<?php include 'koneksi/footer.php' ?>

<script src="../js/nav.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
	crossorigin="anonymous"></script>

<!-- ALL JS FILES -->
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/menu.js"></script>
<!-- ALL PLUGINS -->

</body>

</html>