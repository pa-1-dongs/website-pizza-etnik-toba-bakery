<?php
include_once('../koneksi/koneksi.php');
if(isset($_POST['kirim'])){
	if(isset($_POST['pesanan'])) {
		$nama = trim($_POST['nama']);
		$nomor = trim($_POST['nomor']);
		$alamat = trim($_POST['alamat']);
		$catatan = trim($_POST['catatan']);
		$pesan = "";

		foreach ($_POST['pesanan'] as $pesanan) {
			$jumlah = $_POST['jumlah_' . str_replace(' ', '_', $pesanan)];

			if ($jumlah > 0) {
				$pesan .= "%0D*" . $pesanan . " : " . $jumlah."*";
			}
		}
		
		if($pesan!=""){
			$query = 'SELECT * FROM admin';
            $result_set = $koneksi->query($query);
            $row = $result_set->fetch_assoc();
			$nomor_telepon =  $row['nomor_telepon_admin'];
			header("location:https://api.whatsapp.com/send?phone=$nomor_telepon&text=Halo%20Pizza%20Etnik%20Toba%20Bakery%0D%0DData%20diri%20saya:%20%0DNama:%20*$nama*%0DAlamat%20Alamat%20Pengiriman:%20*$alamat*%0DMenu%20yang%20dipesan:$pesan%0D%0DNOTES:%20*$catatan*");
		} else {
			echo "<script>alert('Mohon pilih minimal satu pesanan.');
			window.location.href = '../menu.php';</script>";
		}
	} else {
		echo "<script>alert('Mohon pilih minimal satu pesanan.');
		window.location.href = '../menu.php';</script>";
	}
}
?>
<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/responsive.css">
	<link rel="stylesheet" href="../css/navbar.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css"
		integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA=="
		crossorigin="anonymous" referrerpolicy="no-referrer" />
		<link href="https://fonts.googleapis.com/css2?family=Magra&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Delius+Unicase&display=swap" rel="stylesheet">
  <style>
	input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
  </style>

<!-- Start Menu -->
<form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
<div class="menu-box">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="heading-title text-center">
					<h2>MENU</h2>
				</div>
			</div>
		</div>

		<div class="row inner-menu-box">
			<div class="col-3">
				<div class="container-input">
					<input type="text" placeholder="Search" name="text" class="input">
					<svg fill="#000000" width="20px" height="20px" viewBox="0 0 1920 1920"
						xmlns="http://www.w3.org/2000/svg">
						<path
							d="M790.588 1468.235c-373.722 0-677.647-303.924-677.647-677.647 0-373.722 303.925-677.647 677.647-677.647 373.723 0 677.647 303.925 677.647 677.647 0 373.723-303.924 677.647-677.647 677.647Zm596.781-160.715c120.396-138.692 193.807-319.285 193.807-516.932C1581.176 354.748 1226.428 0 790.588 0S0 354.748 0 790.588s354.748 790.588 790.588 790.588c197.647 0 378.24-73.411 516.932-193.807l516.028 516.142 79.963-79.963-516.142-516.028Z"
							fill-rule="evenodd"></path>
					</svg>
				</div>
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-pills-makanan-tab" data-toggle="pill" href="#v-pills-makanan" role="tab" aria-controls="v-pills-makanan" aria-selected="true">Makanan</a>
						<a class="nav-link" id="v-pills-minuman-tab" data-toggle="pill" href="#v-pills-minuman" role="tab" aria-controls="v-pills-minuman" aria-selected="false">Minuman</a>
						<a class="nav-link" id="v-pills-roti-tab" data-toggle="pill" href="#v-pills-roti" role="tab" aria-controls="v-pills-roti" aria-selected="false">Roti dan Kue</a>
				</div>
			</div>

			<div class="col-9">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-makanan" role="tabpanel" aria-labelledby="v-pills-makanan-tab">
							<div class="row">
							  <?php 
			                  $query = 'SELECT * FROM produk WHERE status_produk="aktif" AND id_kategori=1';
			                  $result_set = $koneksi->query($query);
			                  while ($row = $result_set->fetch_assoc()){
			                  ?>
			                  <div class="col-lg-4 col-md-6 special-grid drinks">
			                    <div class="gallery-single fix">
			                      <img src="../pictures/<?php echo $row['gambar_produk'];?>" class="img-fluid" alt="Image" />
			                      <div class="why-text">
			                        <h4><?php echo  $row['nama_produk'];?></h4>
			                        <p><?php echo  $row['deskripsi_produk'];?></p>
			                        <h5>Rp <?php echo number_format($row['harga_produk'],0,',','.');?></h5>
			                      </div>
			                    </div>
								<input type="checkbox" id="<?php echo  $row['nama_produk'];?>" name="pesanan[]" value="<?php echo  $row['nama_produk'];?>">Pilih
                                <input type="number" id="<?php echo  $row['nama_produk'];?>" name="jumlah_<?php echo  $row['nama_produk'];?>" value="0" min="0" max="500" style="width: 40px;">
			                  </div>
			                  <?php } ?>
							</div>
							
						</div>
						<div class="tab-pane fade" id="v-pills-minuman" role="tabpanel" aria-labelledby="v-pills-minuman-tab">
							<div class="row">
								  <?php 
			                  $query = 'SELECT * FROM produk WHERE status_produk="aktif" AND id_kategori=2';
			                  $result_set = $koneksi->query($query);
			                  while ($row = $result_set->fetch_assoc()){
			                  ?>
			                  <div class="col-lg-4 col-md-6 special-grid drinks">
			                    <div class="gallery-single fix">
			                      <img src="../pictures/<?php echo $row['gambar_produk'];?>" class="img-fluid" alt="Image" />
			                      <div class="why-text">
			                        <h4><?php echo  $row['nama_produk'];?></h4>
			                        <p><?php echo  $row['deskripsi_produk'];?></p>
			                        <h5>Rp <?php echo number_format($row['harga_produk'],0,',','.');?></h5>
			                      </div>
			                    </div>
								<input type="checkbox" id="<?php echo  $row['nama_produk'];?>" name="pesanan[]" value="<?php echo  $row['nama_produk'];?>">Pilih
                                <input type="number" id="<?php echo  $row['nama_produk'];?>" name="jumlah_<?php echo  $row['nama_produk'];?>" value="0" min="0" max="500" style="width: 40px;">
			                  </div>
			                  <?php } ?>
							</div>
							</div>
							<div class="tab-pane fade" id="v-pills-roti" role="tabpanel" aria-labelledby="v-pills-roti-tab">
							<div class="row">
							  <?php 
			                  $query = 'SELECT * FROM produk WHERE status_produk="aktif" AND id_kategori=3';
			                  $result_set = $koneksi->query($query);
			                  while ($row = $result_set->fetch_assoc()){
			                  ?>
			                  <div class="col-lg-4 col-md-6 special-grid drinks">
			                    <div class="gallery-single fix">
			                      <img src="../pictures/<?php echo $row['gambar_produk'];?>" class="img-fluid" alt="Image" />
			                      <div class="why-text">
			                        <h4><?php echo  $row['nama_produk'];?></h4>
			                        <p><?php echo  $row['deskripsi_produk'];?></p>
			                        <h5>Rp <?php echo number_format($row['harga_produk'],0,',','.');?></h5>
			                      </div>
			                    </div>
								<input type="checkbox" id="<?php echo  $row['nama_produk'];?>" name="pesanan[]" value="<?php echo  $row['nama_produk'];?>">Pilih
                                <input type="number" id="<?php echo  $row['nama_produk'];?>" name="jumlah_<?php echo  $row['nama_produk'];?>" value="0" min="0" max="500" style="width: 40px;">
			                  </div>
			                  <?php } ?>
							</div>
						</div>
						</div>
						</div>
					</div>
				</div>
			</div>

			<!-- form pesan -->
<h1 class="h1">Pesan Sekarang</h1>
<div class="wrapper">
	<input type="text" name="nama" id="" required placeholder="Nama Anda">
	<input type="text" name="alamat" id="" required placeholder="Alamat Lengkap Anda">
	<input type="number" name="nomor" id="" required placeholder="No.Telepon" class="col-lg-12;" style="height:50px;width:100%;" min="0">
	<textarea name="catatan" id="" cols="30" rows="10" placeholder="Catatan (optional)" style="margin-top:3%"></textarea>
	<button name="kirim">
		<p>Kirim</p>
		<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-whatsapp"
			viewBox="0 0 16 16">
			<path
				d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z">
			</path>
		</svg>
	</button>
</div>
<br>
</form>
<a href="menu.php">BATAL</a>
<!-- end of from pesan -->

			<!-- ALL JS FILES -->
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/menu.js"></script>