<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="../Pict/PET.png" type="image/jpg">
  <title>Pizza Etnik Toba Bakery </title>
  <!-- link css -->
  <link rel="stylesheet" href="../css/tentang.css">
  <link rel="stylesheet" href="../css/navbar.css">
  <link href="../css/glightbox.min.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css"
    integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
  <link href="https://fonts.googleapis.com/css2?family=Magra&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Delius+Unicase&display=swap" rel="stylesheet">
  <!-- end of link css -->
</head>

<body>
  <!-- header -->
  <?php include 'koneksi/header.php' ?>

  <!-- sejarah -->
  <h1 class="h1">TENTANG</h1>
  <h2 class="h2">Sejarah</h2>
  <p class="sejarah">Toko Roti Pizza Etnik Toba beroperasi sejak tahun 2015 dengan keinginan untuk memajukan kuliner
    Toba.Pemilik toko memiliki ide untuk menciptakan variasi kuliner yang tak terlupakan bagi para tamu. Penikmat pizza
    ini tidak hanya warga lokal saja, bahkan tamu dari luar kota pun menyukainya. Keahlian suaminya sebagai seorang
    baker
    yang ahli dalam membuat pizza membantu ide dari pemilik toko yaitu ibu Helena J Pakpahan dapat terwujud.
    Daya Tarik produk dari bisnis yang mereka dirikan adalah varian toping dan menu istimewa yang berbahan baku unik
    seperti Dalinihorbo (susu kerbau yang dibekukan),andaliman, mujahir Toba dan Natinuktuk.
    Toko ini mengembangkan bisnisnya dengan menggunakan teknologi berupa media sosial dan website yang mana website
    mencakup info terbaru tentang toko, galeri yang berisi foto produk, kontak, alamat yang dilengkapi dengan rute
    menggunakan Google maps dan feedback dari customer.Namun, toko belum menyediakan pemesanan secara online dan fitur
    untuk penyampaian promosi dan informasi diskon produk. Toko juga memiliki link website awal yang digunakan beberapa
    tahun yang lalu. Berikut adalah link website toko yang lama: <a
      href="https://new-delicious-pizza-etnik-toba.business.site/">Pizza Etnik Toba</a></p>
  <!-- end of sejarah  -->

  <!-- prestasi -->
  <h2 class="h2">Prestasi</h2>
  <div>
  <?php 
                  include_once('koneksi/koneksi.php');
                  // untuk client interface
                  $query = "SELECT * FROM prestasi";
                  $result_set = $koneksi->query($query);
                  $counter = 0;
                  while ($row = $result_set->fetch_assoc()){
                  ?>
    <details <?php if($counter==0) echo "open"; ?>>
      <summary><?php echo $row['judul_prestasi'] ?></summary>
      <div class="grid-container">
        <img src="../pictures/<?php echo $row['gambar_prestasi'] ?>" alt="Deskripsi Gambar" class="image">
        <div class="text-container">
          <p><?php echo $row['deskripsi_prestasi'] ?></p>
        </div>
    </details>
    <?php   $counter++;} ?>
  </div>
  <!-- end of prestasi -->


  <!-- footer -->
  <?php include 'koneksi/footer.php' ?>
  
  <script src="../js/nav.js"></script>
  <!-- footer end -->

  <!-- link js -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
    crossorigin="anonymous"></script>
  <!-- end of link js -->
</body>

</html>