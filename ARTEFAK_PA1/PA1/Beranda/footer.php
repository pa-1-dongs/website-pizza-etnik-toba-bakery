<!-- footer -->
<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="footer-col col-lg-2">
                <a href="../LOGIN/login.php"><img src="../pict/PET.png" style="width: 180px;"></a>
                </div>
                <div class="footer-col col-lg-3">
                    <h4>Find Us</h4>
                    <ul>
                        <li><a href="#"> <img src="../pict/facebook.png" style="width: 20px;"> Pizza Etnik Toba</a></li>
                        <li><a href="#"> <img src="../pict/instagram.png" style="width: 20px;"> Pizza Etnik Toba</a>
                        </li>
                        <li><a href="#"> <img src="../pict/whatsapp.png" style="width:20px;"> +62 813-7508-9145</a></li>
                    </ul>
                </div>
                <div class="footer-col col-lg-3">
                    <h4>Location</h4>
                    <ul>
                        <li><a href="#"> <img src="../pict/gps.png" style="width: 20px;"> Jalan Sisingamangaraja 186
                                22312
                                Balige </a></li>
                        <li><a href="#"> <img src="../Pict/clock.png" style="width:20px;"> Jam Operasi : <br> <br>
                                Setiap
                                Hari <br> 08.00 AM - 22.00 PM</a></li>
                    </ul>
                </div>

            </div>
        </div>

        <center>
            <p style="margin: 0; padding: 0;">Copyright © Pizza Etnik Toba 2023</p>
        </center>
    </footer>