<!DOCTYPE html>
<html lang="en">
<?php include 'navbar.php' ?>
    <!-- body -->

    <main>
        <div class="bg_isi">
            <img src="../Pict/bck4.jpg" class="main">
        </div>

        <!-- carousel -->
        <br>
        <br>
        <center>
            <h1 style="font-family: 'montserrat';font-size: 50px; letter-spacing: 3px; font-weight: 300;">BERANDA</h1>
        </center>
        <br>
        <br>

        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="../Pict/banner1.jpg" class="d-block w-100">
                </div>
                <div class="carousel-item">
                    <img src="../Pict/banner2.jpg" class="d-block w-100">
                </div>
                <div class="carousel-item">
                    <img src="../Pict/banner3.jpg" class="d-block w-100">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

        <section id="about" class="about">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 d-flex justify-content-center video-box align-items-stretch position-relative"
                        style="width: 500px; border: 10px;">
                        <a href="https://www.youtube.com/watch?v=vD4DiGIuu2s" class="glightbox play-btn mb-4"></a>
                    </div>
                    <div class="col-xl-7 col-lg-7 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-7 px-lg-5"
                        style="max-width:100%;">
                        <h3 style="color: white; font-family: 'montserrat'; margin-top: 50px;">Pizza Etnik Toba</h3>
                        <p style="color: white; text-align: justify; padding: 0 0 140px 0; text-indent: 0.8in;">Pizza
                            Etnik Toba Bakery Memberi Pengalaman Makanan Internasional dengan citarasa Nasional.
                            Pizza Etnik Toba Bakery Membawa Olahan Khas Batak dalam Kancah Dunia
                            Kalau Anda mencari kuliner Batak, jangan ragu untuk mengunjungi Kami,
                            Pizza Etnik Toba Bakery</p>
                    </div>
                </div>
            </div>
        </section>
        <div style="padding: 40px 0 0 0;">
            <center>
                <h1 style="font-family: 'montserrat';">What Says Our Customer?</h1>
            </center>
        </div>
        <div class="row">
            <div class="column col-lg-6">
                <div class="card">
                    <span class="tag">Username</span>
                    <p class="comment">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis a laboriosam
                        dolorem culpa rem minima eum id magnam error? Esse itaque obcaecati cum, iure alias aliquam
                        dolore incidunt nesciunt unde?</p>
                </div>
            </div>
            <div class="column col-lg-6">
                <div class="card">
                    <span class="tag">Username</span>
                    <p class="comment">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis a laboriosam
                        dolorem culpa rem minima eum id magnam error? Esse itaque obcaecati cum, iure alias aliquam
                        dolore incidunt nesciunt unde?</p>
                </div>
            </div>
            <div class="column col-lg-6">
                <div class="card">
                    <span class="tag">Username</span>
                    <p class="comment">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis a laboriosam
                        dolorem culpa rem minima eum id magnam error? Esse itaque obcaecati cum, iure alias aliquam
                        dolore incidunt nesciunt unde?</p>
                </div>
            </div>
            <div class="column col-lg-6">
                <div class="card">
                    <span class="tag">Username</span>
                    <p class="comment">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Omnis a laboriosam
                        dolorem culpa rem minima eum id magnam error? Esse itaque obcaecati cum, iure alias aliquam
                        dolore incidunt nesciunt unde?</p>
                </div>
            </div>
        </div>
        <br> <br>
        <center>
            <h2><a style="text-decoration: none; color: var(--color3); font-family: 'montserrat';"
                    href="../Masukan/feedback.html">Lihat
                    Lainnya</a></h2>
        </center>

        <div class="maps">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.507419065683!2d99.06613341467167!3d2.3343405982987155!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e053fb3f7e38d%3A0x4680c8c5c6cba834!2sPizza%20Etnik%20Toba!5e0!3m2!1sid!2sid!4v1679733386145!5m2!1sid!2sid"
                width="100%" height="450" style="border:0; margin-top: -200px; padding: 0;" allowfullscreen=""
                loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </main>
    <?php include 'footer.php' ?>
    <script src="glightbox/js/glightbox.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="script.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.js"></script>
    <script>

        // $(function () {
        //     $("#header").load("../navbar/header.html"); --> include header jquery
        //     $("#footer").load("../navbar/footer.html");
        // });
    </script>

</body>

</html>